FROM ubuntu:16.04
MAINTAINER Phil Weir <phil.weir@flaxandteal.co.uk>
# With acknowledgements to the maintainer of rehabstudio/docker-gunicorn-nginx:
#       Peter McConnell <peter.mcconnell@rehabstudio.com>

# keep upstart quiet
RUN dpkg-divert --local --rename --add /sbin/initctl
RUN ln -sf /bin/true /sbin/initctl

# no tty
ENV DEBIAN_FRONTEND noninteractive

# get up to date
RUN apt-get update --fix-missing

# global installs [applies to all envs!]
RUN apt-get install -y build-essential git
RUN apt-get install -y python3 python3-dev python3-setuptools
RUN apt-get install -y python3-pip python3-scipy python3-numpy

# create a virtual environment and install all depsendecies from pypi
RUN apt-get install -y libyaml-dev
RUN apt-get install -y libhdf5-dev libnetcdf-dev
RUN apt-get install -y libssl-dev python3-flask python3-sqlalchemy python3-pytest

ADD ./requirements.txt /opt/requirements.txt
RUN pip3 install -r /opt/requirements.txt
RUN pip3 install pyyaml nose gunicorn
RUN pip3 install https://github.com/landlab/landlab/zipball/master
RUN pip3 install pyproj
RUN pip3 install airflow

COPY . /opt/app

ENV AIRFLOW_HOME /opt/app

WORKDIR /opt/app

CMD ["airflow", "test", "simulation", "2017-08-10 00:00:00"]
