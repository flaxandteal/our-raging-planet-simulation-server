# http://www.iitk.ac.in/nicee/wcee/article/8_vol1_337.pdf

from math import sqrt, log, exp, log10
from scipy.special import erf
from pyproj import Geod

class QuakeRisk:
    _risk_range = (-1.0, 1.0)

    # maximum expected ground acceleration
    _e = 40000 # 40km - focal depth

    # alpha based on 1/sqrt(2d - 1)
    # for d = 3.0
    # IITK-GSDMA pg16 - nominal Seismic Reduction Factor, R
    _mu = 2.0

    # = 1.8/(T-Tc+0.5) for T>Tc
    # = Sbar(Tc) otherwise
    _nominal_T = 0.45 # roughly 2 storey RC, based on EBB
    _M = 6.0 # Richter scale
    _rock_level = 200
    _nominal_N = 2

    # ~EUROCODES pg20~
    # Geotechnical Risk & Safety (Type 1 [M>5.5])
    _soil = {
         'A': { # rock
             'S': 1.0, # soil factor
             'T_B': 0.15,
             'T_C': 0.4,
             'T_D': 2.0,
         },
         'B': { # 10s of m very dense gravel/sand/clay
             'S': 1.2, # soil factor
             'T_B': 0.15,
             'T_C': 0.5,
             'T_D': 2.0,
         },
         'C': { # 10s-100s of m dense gravel/sand/clay
             'S': 1.15, # soil factor
             'T_B': 0.20,
             'T_C': 0.6,
             'T_D': 2.0,
         },
         'D': { # fairly loose soil
             'S': 1.35, # soil factor
             'T_B': 0.20,
             'T_C': 0.8,
             'T_D': 2.0,
         },
         'E': { # surface alluvium
             'S': 1.4, # soil factor
             'T_B': 0.15,
             'T_C': 0.5,
             'T_D': 2.0,
         }
    }
    _v_s = 0.4
    _v_r = 0.45

    def __init__(self):
        # EBB pg 15 - T < 0.55
        self._Sbar = 1.0 / self._nominal_T

        #EUROCODES pg20
        self._eta = 1.0 # "1.0 for 5%"

        self._geod = Geod(ellps='clrk66')

    def advance_time(self, time):
        pass

    def requires_elevation(self):
        return True

    def load(self, logger, center, z, **parameters):
        self._center = center
        self._logger = logger
        self._A_m = []
        self._rs = []
        self._Ss = []
        self._Pfs = []

    # Are x and y definitely in SI?
    def calculate(self, x, z, i, j):
        # Rudimentary system until we get usable open data for this
        if z < 5:
            soil = self._soil['E']
        elif z < 10:
            soil = self._soil['D']
        elif z < 100:
            soil = self._soil['B']
        else:
            soil = self._soil['A']

        g = 9.81
        T_G = 0.3

        #Kanai 1966 (cf gmpereport2014.pdf)
        a_1 = 5
        #a_1 = 6.74 / sqrt(T_G)
        a_2 = 0.61
        #a_2 = 1.405
        a_3 = 1.66
        #a_3 = 1.73
        a_4 = 3.60
        a_5 = 0.167
        a_6 = -1.83

        elat, elon = self._center

        xlat, xlon = x
        _, _, delta = self._geod.inv(elon, elat, xlon, xlat)

        e = self._e
        r = sqrt(delta * delta + e * e)
        #A_m = a_1 * exp(a_2 * self._M) * ((r / 1000) ** -a_3) / 100.
        rkm = r / 1000
        P = a_3 + a_4 / rkm
        Q = a_5 + a_6 / rkm
        A_m = (a_1 / sqrt(T_G)) * 10 ** (a_2 * self._M - P * log10(rkm) + Q);
        self._A_m.append(A_m)
        self._rs.append(r)

        # Geotechnical Risk & Safety
        # & EURCODE pg19
        if T_G < soil['T_B']:
            factor = 1 + T_G / soil['T_B'] * (self._eta * 2.5 - 1)
        elif T_G < soil['T_C']:
            factor = self._eta * 2.5
        elif T_G < soil['T_D']:
            factor = self._eta * 2.5 * soil['T_C'] / T_G
        else:
            factor = self._eta * 2.5 * soil['T_C'] * soil['T_D'] / (T_G * T_G)
        m_s = A_m * soil['S'] * factor / 100.
        self._Ss.append(m_s)

        #m_s = self._Sbar * soil['ga'] * self._eta * A_m / g
        N = self._nominal_N
        mu = self._mu

        # Onose, 1984
        # http://www.iitk.ac.in/nicee/wcee/article/8_vol4_847.pdf
        v_s = self._v_s
        v_r = self._v_r
        m_r = 4.9899 * (1 / (N * N)) - 0.9645 * (1 / N) + 0.9768
        zez = sqrt(log((1 + v_r * v_r) * (1 + v_s * v_s)))
        laz = log(m_r / m_s) - 0.5 * log((1 + v_r * v_r) / (1 + v_s * v_s))
        al = 1 / sqrt(2 * mu - 1)
        om = (log(al) - laz) / zez

        # %ge damaged buildings to all buildings
        Pf = 0.5 * (1 + erf(om / sqrt(2)))

        # rescale for region of interest
        Pf = min(1, max(0, (Pf - 0.6) / 0.2))
        self._Pfs.append(Pf)
        return Pf

    def finish(self):
        self._logger.info(max(self._A_m))
        self._logger.info(min(self._rs))
        self._logger.info(self._Ss[::1000])
        self._logger.info(self._A_m[::1000])
        self._logger.info(self._Pfs[::1000])
