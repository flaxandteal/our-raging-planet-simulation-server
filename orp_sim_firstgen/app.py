#This class is used for creating an object with type app
from flask import Flask
from flask_restful import Resource, Api, reqparse
import os
import numpy as N
import json
import time
import redis
from .result_service import ResultService, Result


def create_app(): # This function is used for creating an app object. Will return an object of type app.
    app = Flask(__name__) # Creating application object, standard for setting up Flask
    api = Api(app) # Setting up the Flask API

    redis_args = {   # Dictionary object that contains the host, port, timeout for the socket and the database. Used for connection
        'host': os.environ['REDIS_HOST'],  #Host for connection
        'port': os.environ['REDIS_PORT'],  #Port for connection
        'socket_timeout': 1,  #Time before timeout for socket
        'db': 0  #Database
    }
    if 'REDIS_PASS' in os.environ:  #If the password for Redis is in the os.environ array, do this.
        redis_args['password'] = os.environ['REDIS_PASS']  #password from redis_args becomes password from os.environ

    _redis = redis.StrictRedis(**redis_args)  # Keyword expansion for dict object

    result_service = ResultService()  # creating new ResultService object
    api.add_resource(Result, '/result/<result_id>', resource_class_kwargs={'service': result_service, 'redis': _redis})  #Add resource to the API?
    result_service.load()   #Call load function from result_service object

    return app      #return app object
