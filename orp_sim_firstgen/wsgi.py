#!/usr/bin/env python
'''
Copyright (c) 2015 Jesse Peterson
Licensed under the MIT license. See the included LICENSE.txt file for details.
'''

from . import app as appm
import logging


def setup(): # Set up function
    app = appm.create_app()  # Creating an app object
    app.logger.addHandler(logging.StreamHandler()) # Adding handler/logger to app
    app.logger.setLevel(logging.INFO) # Set level of logger to INFO
    return app # return app object


app = setup() # call setup method on app object
if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0')
