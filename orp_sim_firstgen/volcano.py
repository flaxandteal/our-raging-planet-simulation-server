# http://www.iitk.ac.in/nicee/wcee/article/8_vol1_337.pdf

from math import sqrt, log, exp, log10
from scipy.special import erf
from pyproj import Geod

class VolcanoConcentricRisk: # This class is used for the Volcano risk
    _risk_range = (50, 500) # Range of the risk
    _risk_time_ratio = 0.001 # Time of the risk
    _time = 0 # Time of the risk

    def __init__(self):
        # gives us a more rectilinear frame of ref
        self._converter = utils.IrishGridConverter()
        self._epicenter = []

    def requires_elevation(self): # Does this risk require elevation? If so it returns true, if not false
        return False

    def advance_time(self, time): # Function for setting time
        self._time = time # Place time parameter into self time parameter

    def load(self, logger, center, z, **parameters): # Loading function....
        logger.info("Starting risk range: %f -> %f" % self._risk_range)
        self._logger = logger
        self._epicenter = self._converter(*center, inverse=False)
        self._parameters = parameters

    # Are x and y definitely in SI?
    def calculate(self, x, z, i, j):
        x = self._converter(*x, inverse=False)
        health = 1.0

        r1 = self._risk_range[0] + self._time * self._risk_time_ratio
        r2 = self._risk_range[1] + self._time * self._risk_time_ratio

        distance_squared = (x[0] - self._epicenter[0]) ** 2 + (x[1] - self._epicenter[1]) ** 2
        # Do we want quadratic scaling?
        health = min(1.0, max(0.0, (distance_squared - self._risk_range[0] ** 2) / (self._risk_range[1] ** 2 - self._risk_range[0] ** 2)))

        return 1 - health

    def finish(self):
        pass
